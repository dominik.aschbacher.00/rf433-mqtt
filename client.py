# MQTT Client

import paho.mqtt.client as mqtt
import subprocess

# Configuration
mqtt_broker = ""
mqtt_username = ""
mqtt_password = ""
mqtt_topic_subscribe = "cmnd/rf-bridge/POWER"
mqtt_topic_publish = "stat/rf-bridge/RESULT"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() - if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(mqtt_topic_subscribe)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    args = msg.payload.split()
    subprocess.call(["./send", args[0], args[1], args[2]])
    client.publish(mqtt_topic_publish, msg.payload)

# Create an MQTT client and attach routines
client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqtt_broker, 1883, 60)
client.loop_forever()
