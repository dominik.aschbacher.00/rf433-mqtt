# RF433-MQTT

This repository contains an mqtt client, that is able to send 433MHz signals.

The following parameters can be changed inside the `client.py` file:

* mqtt broker
* mqtt username
* mqtt password
* mqtt subscribe topic
* mqtt publish topic

## Test 433MHz modules

Both transmitter and reciever must be powered with 3.3V and require a connection to GND. For the transmitter the default gpio pin is `GPIO 17` and for the reciever it is `GPIO 27`.

The gpio pin used for data can also be set to a custom pin. Therefore add the argument `-g <GPIO_PIN>` to either the send or recieve script.

To listen for 433MHz signals use the following command:

     python3 test_recieve.py

To send specific messages use the following command:

     python3 test_send.py -p <PULSE_LENGTH> -t <PROTOCOL> <CODE>